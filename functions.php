<?php
/**
 * Modu-eco
 * Funções e Definições
 *
 * Tema WordPress voltado para o site institucional da Modu-eco.
 *
 * Para auxílio em futuras implementações dentro da plataforma do wordpress, acesse http://codex.wordpress.org/.
 *
 * @package WordPress 3.8+
 * @author Emiliano Matusumura e Rubens Fidelis
 * @version 1.0 (01/02/2014)
**/

// Habilitando relatório de erros do PhP
error_reporting( E_ALL );
ini_set( "display_errors", "1" );

// Desabilita edição de arquivos do tema dentro do WordPress
define( "DISALLOW_FILE_EDIT", true );

// Importando recursos de layout do tema
add_action( "wp_enqueue_scripts", "header_assets" );
function header_assets() {
	wp_enqueue_style( "css-font", "http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light", array(), null, "all" );
	wp_enqueue_style( "css-font-awesome", get_template_directory_uri() . "/css/fonts/font-awesome/css/font-awesome.css", array(), null, "all" );
	wp_enqueue_style( "css-bootstrap", get_template_directory_uri() . "/css/bootstrap.css", array(), null, "all" );
	wp_enqueue_style( "css-magnific-popup", get_template_directory_uri() . "/vendor/magnific-popup/magnific-popup.css", array(), null, "all" );
	wp_enqueue_style( "css-theme", get_template_directory_uri() . "/css/theme.css", array(), null, "all" );
	wp_enqueue_style( "css-theme-elements", get_template_directory_uri() . "/css/theme-elements.css", array(), null, "all" );
	wp_enqueue_style( "css-theme-animate", get_template_directory_uri() . "/css/theme-animate.css", array(), null, "all" );
	wp_enqueue_style( "css-slider-revolution", get_template_directory_uri() . "/vendor/rs-plugin/css/settings.css", array(), null, "all" );
	wp_enqueue_style( "css-circle-flip", get_template_directory_uri() . "/vendor/circle-flip-slideshow/css/component.css", array(), null, "all" );
	wp_enqueue_style( "css-skin", get_template_directory_uri() . "/css/skins/modu-eco.css", array(), null, "all" );
	wp_enqueue_style( "css-custom", get_template_directory_uri() . "/css/custom.css", array(), null, "all" );
	wp_enqueue_script( "js-modernizr", get_template_directory_uri() . "/vendor/modernizr.js", array(), null, false );
}
add_action( "wp_enqueue_scripts", "footer_assets" );
function footer_assets() {
	wp_enqueue_script( "js-jquery", get_template_directory_uri() . "/vendor/jquery.js", array(), null, true );
	wp_enqueue_script( "js-plugins", get_template_directory_uri() . "/js/plugins.js", array(), null, true );
	wp_enqueue_script( "js-jquery-easing", get_template_directory_uri() . "/vendor/jquery.easing.js", array(), null, true );
	wp_enqueue_script( "js-jquery-appear", get_template_directory_uri() . "/vendor/jquery.appear.js", array(), null, true );
	wp_enqueue_script( "js-jquery-cookie", get_template_directory_uri() . "/vendor/jquery.cookie.js", array(), null, true );
	wp_enqueue_script( "js-bootstrap", get_template_directory_uri() . "/vendor/bootstrap.js", array(), null, true );
	wp_enqueue_script( "js-slider-revolution-plugin", get_template_directory_uri() . "/vendor/rs-plugin/js/jquery.themepunch.plugins.min.js", array(), null, true );
	wp_enqueue_script( "js-slider-revolution", get_template_directory_uri() . "/vendor/rs-plugin/js/jquery.themepunch.revolution.js", array(), null, true );
	wp_enqueue_script( "js-circle-flip", get_template_directory_uri() . "/vendor/circle-flip-slideshow/js/jquery.flipshow.js", array(), null, true );
	wp_enqueue_script( "js-magnific-popup", get_template_directory_uri() . "/vendor/magnific-popup/magnific-popup.js", array(), null, true );
	wp_enqueue_script( "js-jquery-validate", get_template_directory_uri() . "/vendor/jquery.validate.js", array(), null, true );

	wp_enqueue_script( "js-home", get_template_directory_uri() . "/js/views/view.home.js", array(), null, true );

	wp_enqueue_script( "js-theme", get_template_directory_uri() . "/js/theme.js", array(), null, true );
	wp_enqueue_script( "js-custom", get_template_directory_uri() . "/js/custom.js", array(), null, true );
}

/**
 * Modifica a variável de paginação nos links de 'page' para 'pg'
**/
$GLOBALS["wp_rewrite"]->pagination_base = "pg";

/**
 * Registra menus a serem utilizados no tema
**/
register_nav_menu( "global", "Menu global" );

/**
 * Walker personalizado dos menus para exibição no formato do Bootstrap
**/
class bootstrap_nav_walker extends Walker_Nav_Menu {
	function start_lvl(&$output, $depth) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class=\"dropdown-menu\">\n";
	}
	function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
		global $wp_query;
		$indent = ($depth) ? str_repeat("\t", $depth) : '';
		if (strcasecmp($item->title, 'divider')) {
			$class_names = $value = '';
			$classes = empty( $item->classes ) ? array() : (array) $item->classes;
			$classes[] = ($item->current) ? 'active' : '';
			$classes[] = 'menu-item-' . $item->ID;
			$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
			if ($args->has_children && $depth > 0) {
				$class_names .= ' dropdown-submenu';
			} else if($args->has_children && $depth === 0) {
				$class_names .= ' dropdown';
			}
			$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
			$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
			$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
			$output .= $indent . '<li' . $id . $value . $class_names .'>';
			$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
			$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
			$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
			$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
			$attributes .= ($args->has_children)        ? ' data-toggle="dropdown" data-target="#" class="dropdown-toggle"' : '';
			$item_output = $args->before;
			$item_output .= '<a'. $attributes .'>';
			$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
			$item_output .= ($args->has_children && $depth == 0) ? ' <span class="caret"></span></a>' : '</a>';
			$item_output .= $args->after;
			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		} else {
			$output .= $indent . '<li class="divider">';
		}
	}
	function display_element($element, &$children_elements, $max_depth, $depth=0, $args, &$output) {
		if (!$element) {
			return;
		}
		$id_field = $this->db_fields['id'];
		if (is_array($args[0])) {
			$args[0]['has_children'] = ! empty( $children_elements[$element->$id_field] );
		} else if (is_object($args[0])) {
			$args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
		}
		$cb_args = array_merge( array(&$output, $element, $depth), $args);
		call_user_func_array(array(&$this, 'start_el'), $cb_args);
		$id = $element->$id_field;
		if (($max_depth == 0 || $max_depth > $depth+1 ) && isset( $children_elements[$id])) {
			foreach ($children_elements[ $id ] as $child) {
				if (!isset($newlevel)) {
					$newlevel = true;
					$cb_args = array_merge( array(&$output, $depth), $args);
					call_user_func_array(array(&$this, 'start_lvl'), $cb_args);
				}
				$this->display_element($child, $children_elements, $max_depth, $depth + 1, $args, $output);
			}
			unset($children_elements[$id]);
		}
		if (isset($newlevel) && $newlevel){
			$cb_args = array_merge(array(&$output, $depth), $args);
			call_user_func_array(array(&$this, 'end_lvl'), $cb_args);
		}
		$cb_args = array_merge(array(&$output, $element, $depth), $args);
		call_user_func_array(array(&$this, 'end_el'), $cb_args);
	}
}

/**
 * Walker personalizado dos menus para exibição no formato de Select (Dropdown para Tablets e Mobile)
**/
class select_nav_walker extends Walker_Nav_Menu {
	var $to_depth = -1;
	function start_lvl(&$output, $depth){
		$output .= '</option>';
	}
	function end_lvl(&$output, $depth){
		$indent = str_repeat("\t", $depth);
	}
	function start_el(&$output, $item, $depth, $args){
		$indent = ( $depth ) ? str_repeat( "&nbsp;", $depth * 4 ) : '';
		$class_names = $value = '';
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';
		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';
		$value = ' value="'. $item->url .'"';
		$output .= '<option'.$id.$value.$class_names.'>';
		$item_output = $args->before;
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$output .= $indent.$item_output;
	}
	function end_el(&$output, $item, $depth){
		if(substr($output, -9) != '</option>') {
			$output .= "</option>";
		}
	}
}

/**
 * Configura tamanhos padrões de imagem do tema
**/
add_theme_support( "post-thumbnails", array( "post", "destaque", "produto" ) );
function remove_image_sizes( $sizes ) {
	unset( $sizes["medium"] );
	unset( $sizes["large"] );
	return $sizes;
}
add_filter( "intermediate_image_sizes_advanced", "remove_image_sizes");
set_post_thumbnail_size(150, 9999, false);
add_image_size("350x350", 350, 350, true);
add_image_size("350wide", 350, 9999, false);
add_image_size("610wide", 610, 9999, false);
function add_custom_media_sizes() {
	$sizes = array( "350wide" => __("Imagem Média"), "610wide" => __("Imagem Grande") );
	return $sizes;
}
add_filter("image_size_names_choose", "add_custom_media_sizes");

/**
 * Função que gera paginação com a estrutura e classes do Bootstrap
**/
function pagination( $_pages = "", $_range = 4 ){
	$_showitems = ( $_range * 2 ) + 1;
	global $_paged;
	if( empty( $_paged ) ) {
		$_paged = 1;
	}
	if( $_pages == "" ) {
		global $wp_query;
		$_pages = $wp_query->max_num_pages;
		if( !$_pages ) {
			$_pages = 1;
		}
	}
	if( 1 != $_pages ) {
		echo "<ul class=\"pagination pagination-lg pull-right\">";
		if( $_paged > 2 && $_paged > $_range + 1 && $_showitems < $_pages ) {
			echo "<li><a href=\"" . get_pagenum_link( 1 ) . "\">&laquo; Primeira</a></li>";
		}
		if( $_paged > 1 && $_showitems < $_pages ) {
			echo "<li><a href=\"" . get_pagenum_link( $_paged - 1 ) . "\">&lsaquo;</a></li>";
		}
		for( $i=1; $i <= $_pages; $i++ ) {
			if( 1 != $_pages && ( !( $i >= $_paged + $_range + 1 || $i <= $_paged - $_range - 1 ) || $_pages <= $_showitems ) ) {
				echo ( $_paged == $i ) ? "<li class=\"active\"><a href=\"#\">$i</a></li>":"<li><a href=\"" . get_pagenum_link( $i ) . "\">$i</a></li>";
			}
		}
		if( $_paged < $_pages && $_showitems < $_pages ) {
			echo "<li><a href=\"" . get_pagenum_link( $_paged + 1 ) . "\">&rsaquo;</a></li>";
		}
		if( $_paged < $_pages - 1 &&  $_paged + $_range - 1 < $_pages && $_showitems < $_pages ) {
			echo "<li><a href='" . get_pagenum_link( $_pages ) . "'>Última &raquo;</a></li>";
		}
		echo "</ul>\n";
	}
}

/**
 * Limita resumos do wordpress a quantidade de palavras especificadas
**/
function resumo( $_string, $_word_limit ) {
	$_words = explode(" ", $_string, ($_word_limit + 1));
	if ( count( $_words ) > $_word_limit )
		array_pop( $_words );
	return implode( " ", $_words );
}

/**
 * The Slug
**/
function the_slug() {
	global $post;
	$post_data = get_post( $post->ID, ARRAY_A );
	$slug = $post_data["post_name"];
	return $slug;
}

/**
 * Cria páginas específicas do tema caso não existam com base no 'Nome'
**/
function verifica_pagina( $_slug, $_nome ) {
	if ( !get_page_by_title( $_nome ) ) {
		wp_insert_post( array( "post_type" => "page", "post_author" => 1, "post_title" => $_nome, "post_name" => $_slug, "post_content" => "...", "coment_status" => "closed", "post_status" => "publish" ) );
	}
}
verifica_pagina( "sobre", "Sobre" );
verifica_pagina( "produtos", "Produtos" );
verifica_pagina( "contato", "Contato" );
verifica_pagina( "fabricacao", "Fabricação" );
verifica_pagina( "tipos", "Tipos" );
verifica_pagina( "aplicacao", "Aplicação" );

/**
 * Verifica valor de input (checkbox e selects) para selecionar valores salvos
**/
function verifica_valor_input( $_comparar, $_valor, $_tipo = "select" ) {
	$_comparar = ( string )$_comparar;
	if( $_tipo == "checkbox" || $_tipo == "radio" ) echo ( $_valor == $_comparar ) ? " checked=\"checked\"" : "";
	if( $_tipo == "select" ) echo ( $_valor == $_comparar ) ? " selected=\"selected\"" : "";
}

/**
 * Filtro de buscas
**/
add_action( "pre_get_posts", "customize_search_display" );
function customize_search_display( $_query ) {
	if ( ( $_query->is_main_query() ) && ( is_search() ) ) {
		$_query->set( "posts_per_page", 10 );
	}
}

/**
 * Tipos de posts personalizados
**/
// add_action( "init", "register_cpt_produtos" );
// function register_cpt_produtos() {
// 	register_post_type( "produtos", array(
// 		"labels"				 => array(
// 			"name"				 => "Produtos",
// 			"singular_name"		 => "Produto",
// 			"add_new"			 => "Novo Produto",
// 			"all_items"			 => "Todos Produtos",
// 			"add_new_item"		 => "Adicionar Produto",
// 			"edit"				 => "Alterar Produto",
// 			"edit_item"			 => "Alterar Produto",
// 			"new_item"			 => "Novo Produto",
// 			"view"				 => "Visualizar Produto",
// 			"view_item"			 => "Visualizar Produto",
// 			"search_items"		 => "Buscar Produtos",
// 			"not_found"			 => "Nenhum resultado encontrado",
// 			"not_found_in_trash" => "Nenhum resultado encontrado na lixeira",
// 		),
// 		"public"					=> true,
// 		"exclude_from_search"		=> true,
// 		"publicly_queryable"		=> true,
// 		"show_ui"					=> true,
// 		"show_in_nav_menus"			=> true,
// 		"show_in_menu"				=> true,
// 		"show_in_admin_bar"			=> true,
// 		"menu_position"				=> 5,
// 		"menu_icon"					=> null,
// 		"capability_type"			=> "post",
// 		"hierarchical"				=> false,
// 		"supports"					=> array( "title", "editor", "thumbnail" ),
// 		"taxonomies"				=> array(),
// 		"rewrite"					=> array( "slug" => "produto" ),
// 		"query_var"					=> "produto",
// 		"can_export"				=> true,
// 	));
// }

/**
 * Página de opções
**/
function rawJSON( $filename ) {
	$_data = file_get_contents( get_template_directory() . "/json/" . $filename );
	$data = json_decode( $_data );
	return $data;
}
function saveJSON( $jsondata, $filename ) {
	$fp = fopen( get_template_directory() . "/json/" . $filename , "w+" );
	fwrite( $fp, json_encode( $jsondata ) );
	fclose( $fp );
	return true;
}
add_action( "admin_menu", "modueco_settings_page_init");
function modueco_settings_page_init() {
	$theme_data = get_theme_data( TEMPLATEPATH."/style.css" );
	$settings_page = add_theme_page( $theme_data["Name"]." - Opções", "Opções", "edit_theme_options", "theme-settings", "modueco_settings_page" );
}
function modueco_admin_tabs( $current = "destaques" ) {
	$tabs = array(
		"destaques"			=> "Destaques",
		"vantagens"			=> "Vantagens",
		/*"servicos"			=> "Serviços",*/
		"contato"			=> "Contato"
	);
	$links = array();
	echo "<div id=\"icon-themes\" class=\"icon32\"><br></div>";
	echo "<h2 class=\"nav-tab-wrapper\">";
	foreach( $tabs as $tab => $name ){
		$class = ( $tab == $current ) ? " nav-tab-active" : "";
		echo "<a class=\"nav-tab$class\" href=\"?page=theme-settings&tab=$tab\">$name</a>";
	}
	echo "</h2>";
}
function modueco_settings_page() {
	global $pagenow;
	$theme_data = get_theme_data( TEMPLATEPATH . "/style.css" );
	$opcoes = rawJSON( "settings.json" );
	$tab = ( isset ( $_GET["tab"] ) ) ? $_GET["tab"] : "destaques";
	if( isset( $_POST["modueco-settings-submit"] ) ) {
		switch( $tab ) {
			case "destaques":
				$opcoes->destaques->fabricacao = ( !empty( $_POST["fabricacao"] ) ) ? $_POST["fabricacao"] : $opcoes->destaques->fabricacao;
				$opcoes->destaques->link_fabricacao = ( !empty( $_POST["link_fabricacao"] ) ) ? $_POST["link_fabricacao"] : $opcoes->destaques->link_fabricacao;
				$opcoes->destaques->tipos = ( !empty( $_POST["tipos"] ) ) ? $_POST["tipos"] : $opcoes->destaques->tipos;
				$opcoes->destaques->link_tipos = ( !empty( $_POST["link_tipos"] ) ) ? $_POST["link_tipos"] : $opcoes->destaques->link_tipos;
				$opcoes->destaques->aplicacao = ( !empty( $_POST["aplicacao"] ) ) ? $_POST["aplicacao"] : $opcoes->destaques->aplicacao;
				$opcoes->destaques->link_aplicacao = ( !empty( $_POST["link_aplicacao"] ) ) ? $_POST["link_aplicacao"] : $opcoes->destaques->link_aplicacao;
				$opcoes->destaques->produtos = ( !empty( $_POST["produtos"] ) ) ? $_POST["produtos"] : $opcoes->destaques->produtos;
				$opcoes->destaques->link_produtos = ( !empty( $_POST["link_produtos"] ) ) ? $_POST["link_produtos"] : $opcoes->destaques->link_produtos;
				break;
			case "vantagens":
				$opcoes->vantagens->descricao = ( !empty( $_POST["descricao_vantagens"] ) ) ? $_POST["descricao_vantagens"] : $opcoes->vantagens->descricao;
				$opcoes->vantagens->titulo = ( !empty( $_POST["titulos_vantagens"] ) ) ? $_POST["titulos_vantagens"] : $opcoes->vantagens->titulo;
				$opcoes->vantagens->porcentagem = ( !empty( $_POST["porcentagens_vantagens"] ) ) ? $_POST["porcentagens_vantagens"] : $opcoes->vantagens->porcentagem;
				$opcoes->vantagens->link = ( !empty( $_POST["link_vantagens"] ) ) ? $_POST["link_vantagens"] : $opcoes->vantagens->link;
				break;
			/*case "servicos":
				$opcoes->servicos->titulo = ( !empty( $_POST["titulos_servicos"] ) ) ? $_POST["titulos_servicos"] : $opcoes->servicos->titulo;
				$opcoes->servicos->descricao = ( !empty( $_POST["descricao_servicos"] ) ) ? $_POST["descricao_servicos"] : $opcoes->servicos->descricao;
				$opcoes->servicos->link = ( !empty( $_POST["link_servicos"] ) ) ? $_POST["link_servicos"] : $opcoes->servicos->link;
				break;*/
			case "contato":
				$opcoes->telefone = ( !empty( $_POST["telefone"] ) ) ? $_POST["telefone"] : $opcoes->telefone;
				$opcoes->email = ( !empty( $_POST["email"] ) ) ? $_POST["email"] : $opcoes->email;
				$opcoes->rua = ( !empty( $_POST["rua"] ) ) ? $_POST["rua"] : $opcoes->rua;
				$opcoes->numero = ( !empty( $_POST["numero"] ) ) ? $_POST["numero"] : $opcoes->numero;
				$opcoes->complemento = ( !empty( $_POST["complemento"] ) ) ? $_POST["complemento"] : $opcoes->complemento;
				$opcoes->bairro = ( !empty( $_POST["bairro"] ) ) ? $_POST["bairro"] : $opcoes->bairro;
				$opcoes->cidade = ( !empty( $_POST["cidade"] ) ) ? $_POST["cidade"] : $opcoes->cidade;
				$opcoes->estado = ( !empty( $_POST["estado"] ) ) ? $_POST["estado"] : $opcoes->estado;
				$opcoes->cep = ( !empty( $_POST["cep"] ) ) ? $_POST["cep"] : $opcoes->cep;
				break;
		}
		saveJSON( $opcoes, "settings.json" );
	} ?>
	<div class="wrap">
		<h2><?php echo $theme_data["Name"]; ?> - Opções</h2> <?php
		if ( isset( $_GET["updated"] ) && "true" == esc_attr( $_GET["updated"] ) ) echo "<div class=\"updated\"><p>Opções atualizadas.</p></div>";
		if ( isset ( $_GET["tab"] ) ) modueco_admin_tabs($_GET["tab"]); else modueco_admin_tabs("destaques"); ?>
		<div id="poststuff">
			<form method="post" action="<?php admin_url( "themes.php?page=theme-settings" ); ?>"> <?php
				if ( $pagenow == "themes.php" && $_GET["page"] == "theme-settings" ){
					echo "<table class=\"form-table\">";
					switch ( $tab ){
						case "destaques" : ?>
							<tr style="border-bottom:1px solid #d9d9d9;padding-bottom: 10px">
								<th><b>Fabricação</b></th>
								<td>
									<div>
										<button id="cmu-fabricacao" type="button" class="button-primary">Selecionar</button> <?php
										if( !empty( $opcoes->destaques->fabricacao ) ) { ?>
											<a class="button" href="<?php echo $opcoes->destaques->fabricacao; ?>" target="_blank">Ver Imagem</a> <?php
										} ?>
										<input class="cmu-fabricacao" type="hidden" name="fabricacao" value="<?php echo $opcoes->destaques->fabricacao; ?>">
									</div>
									<div style="margin-top:5px">
										<label>
											Link <small>(completo, <b>com http://</b>. Se vazio, usar <b>#</b>)</small><br>
											<input type="text" name="link_fabricacao" style="width:100%" value="<?php echo $opcoes->destaques->link_fabricacao; ?>">
										</label>
									</div>
								</td>
							</tr>
							<tr style="border-bottom:1px solid #d9d9d9;padding-bottom: 10px">
								<th><b>Tipos</b></th>
								<td>
									<div>
										<button id="cmu-tipos" type="button" class="button-primary">Selecionar</button> <?php
										if( !empty( $opcoes->destaques->tipos ) ) { ?>
											<a class="button" href="<?php echo $opcoes->destaques->tipos; ?>" target="_blank">Ver Imagem</a> <?php
										} ?>
										<input class="cmu-tipos" type="hidden" name="tipos" value="<?php echo $opcoes->destaques->tipos; ?>">
									</div>
									<div style="margin-top:5px">
										<label>
											Link <small>(completo, <b>com http://</b>. Se vazio, usar <b>#</b>)</small><br>
											<input type="text" name="link_tipos" style="width:100%" value="<?php echo $opcoes->destaques->link_tipos; ?>">
										</label>
									</div>
								</td>
							</tr>
							<tr style="border-bottom:1px solid #d9d9d9;padding-bottom: 10px">
								<th><b>Aplicação</b></th>
								<td>
									<div>
										<button id="cmu-aplicacao" type="button" class="button-primary">Selecionar</button> <?php
										if( !empty( $opcoes->destaques->aplicacao ) ) { ?>
											<a class="button" href="<?php echo $opcoes->destaques->aplicacao; ?>" target="_blank">Ver Imagem</a> <?php
										} ?>
										<input class="cmu-aplicacao" type="hidden" name="aplicacao" value="<?php echo $opcoes->destaques->aplicacao; ?>">
									</div>
									<div style="margin-top:5px">
										<label>
											Link <small>(completo, <b>com http://</b>. Se vazio, usar <b>#</b>)</small><br>
											<input type="text" name="link_aplicacao" style="width:100%" value="<?php echo $opcoes->destaques->link_aplicacao; ?>">
										</label>
									</div>
								</td>
							</tr> <?php
							for( $i = 0; $i < 4; $i++ ) { ?>
								<tr style="border-bottom:1px solid #d9d9d9;padding-bottom: 10px">
									<th><b>Produtos #<?php echo $i + 1; ?></b></th>
									<td>
										<div>
											<button id="cmu-produtos-<?php echo $i + 1; ?>" type="button" class="button-primary">Selecionar</button> <?php
											if( !empty( $opcoes->destaques->produtos[$i] ) ) { ?>
												<a class="button" href="<?php echo $opcoes->destaques->produtos[$i]; ?>" target="_blank">Ver Imagem</a> <?php
											} ?>
											<input class="cmu-produtos-<?php echo $i + 1; ?>" type="hidden" name="produtos[]" value="<?php echo $opcoes->destaques->produtos[$i]; ?>">
										</div>
										<div style="margin-top:5px">
											<label>
												Link <small>(completo, <b>com http://</b>. Se vazio, usar <b>#</b>)</small><br>
												<input type="text" name="link_produtos[]" style="width:100%" value="<?php echo $opcoes->destaques->link_produtos[$i]; ?>">
											</label>
										</div>
									</td>
								</tr> <?php
							}
						break;
						case "vantagens" : ?>
							<tr style="border-bottom:1px solid #d9d9d9">
								<th><b>Descrição geral</b></th>
								<td>
									<div>
										<textarea name="descricao_vantagens" style="width:100%" rows="10"><?php echo $opcoes->vantagens->descricao; ?></textarea>
									</div>
								</td>
							</tr> <?php
							for( $i = 0; $i < 4; $i++ ) { ?>
								<tr style="border-bottom:1px solid #d9d9d9">
									<th><b>Vantagem #<?php echo $i + 1; ?></b></th>
									<td>
										<div>
											<label>
												Título<br>
												<input type="text" name="titulos_vantagens[]" style="width:100%" value="<?php echo $opcoes->vantagens->titulo[$i]; ?>">
											</label>
										</div>
										<div style="margin-top:5px;padding-bottom:5px;border-bottom:1px solid #e9e9e9">
											<label>
												Porcentagem (somente números)<br>
												<input maxlength="3" type="text" name="porcentagens_vantagens[]" style="width:100%" value="<?php echo $opcoes->vantagens->porcentagem[$i]; ?>">
											</label>
										</div>
										<div>
											<label>
												Link <small>(completo, <b>com http://</b>. Se vazio, usar <b>#</b>)</small><br>
												<input type="text" name="link_vantagens[]" style="width:100%" value="<?php echo $opcoes->vantagens->link[$i]; ?>">
											</label>
										</div>
									</td>
								</tr><?php
							}
						break;
						/*case "servicos" :
							for( $i = 0; $i < 3; $i++ ) { ?>
								<tr style="border-bottom:1px solid #d9d9d9">
									<th><b>Serviço #<?php echo $i + 1; ?></b></th>
									<td>
										<div>
											<label>
												Título<br>
												<input type="text" name="titulos_servicos[]" style="width:100%" value="<?php echo $opcoes->servicos->titulo[$i]; ?>">
											</label>
										</div>
										<div style="margin-top:5px">
											<label>
												Descrição<br>
												<textarea name="descricao_servicos[]" style="width:100%" rows="3"><?php echo $opcoes->servicos->descricao[$i]; ?></textarea>
											</label>
										</div>
										<div style="margin-top:5px">
											<label>
												Link <small>(completo, <b>com http://</b>. Se vazio, usar <b>#</b>)</small><br>
												<input type="text" name="link_servicos[]" style="width:100%" value="<?php echo $opcoes->servicos->link[$i]; ?>">
											</label>
										</div>
									</td>
								</tr><?php
							}
						break;*/
						case "contato" : ?>
							<tr style="border-bottom:1px solid #eeeeee">
								<th><b>Contato</b></th>
								<td>
									<label>
										Telefone<br>
										<input type="text" name="telefone" style="width:100%" value="<?php echo $opcoes->telefone; ?>">
									</label>
									<label>
										Email<br>
										<input type="text" name="email" style="width:100%" value="<?php echo $opcoes->email; ?>">
									</label>
									<label>
										Rua<br>
										<input type="text" name="rua" style="width:100%" value="<?php echo $opcoes->rua; ?>">
									</label>
									<label>
										Número<br>
										<input type="text" name="numero" style="width:100%" value="<?php echo $opcoes->numero; ?>">
									</label>
									<label>
										Complemento<br>
										<input type="text" name="complemento" style="width:100%" value="<?php echo $opcoes->complemento; ?>">
									</label>
									<label>
										Bairro<br>
										<input type="text" name="bairro" style="width:100%" value="<?php echo $opcoes->bairro; ?>">
									</label>
									<label>
										Cidade<br>
										<input type="text" name="cidade" style="width:100%" value="<?php echo $opcoes->cidade; ?>">
									</label>
									<label>
										Estado<br>
										<select style="width:100%" name="estado">
										    <option value="AC" <?php echo ( $opcoes->estado == "AC" ) ? "selected=\"selected\"" : ""; ?>>Acre</option>
										    <option value="AL" <?php echo ( $opcoes->estado == "AL" ) ? "selected=\"selected\"" : ""; ?>>Alagoas</option>
										    <option value="AP" <?php echo ( $opcoes->estado == "AP" ) ? "selected=\"selected\"" : ""; ?>>Amapá</option>
										    <option value="AM" <?php echo ( $opcoes->estado == "AM" ) ? "selected=\"selected\"" : ""; ?>>Amazonas</option>
										    <option value="BA" <?php echo ( $opcoes->estado == "BA" ) ? "selected=\"selected\"" : ""; ?>>Bahia</option>
										    <option value="CE" <?php echo ( $opcoes->estado == "CE" ) ? "selected=\"selected\"" : ""; ?>>Ceará</option>
										    <option value="DF" <?php echo ( $opcoes->estado == "DF" ) ? "selected=\"selected\"" : ""; ?>>Distrito Federal</option>
										    <option value="ES" <?php echo ( $opcoes->estado == "ES" ) ? "selected=\"selected\"" : ""; ?>>Espirito Santo</option>
										    <option value="GO" <?php echo ( $opcoes->estado == "GO" ) ? "selected=\"selected\"" : ""; ?>>Goiás</option>
										    <option value="MA" <?php echo ( $opcoes->estado == "MA" ) ? "selected=\"selected\"" : ""; ?>>Maranhão</option>
										    <option value="MS" <?php echo ( $opcoes->estado == "MS" ) ? "selected=\"selected\"" : ""; ?>>Mato Grosso do Sul</option>
										    <option value="MT" <?php echo ( $opcoes->estado == "MT" ) ? "selected=\"selected\"" : ""; ?>>Mato Grosso</option>
										    <option value="MG" <?php echo ( $opcoes->estado == "MG" ) ? "selected=\"selected\"" : ""; ?>>Minas Gerais</option>
										    <option value="PA" <?php echo ( $opcoes->estado == "PA" ) ? "selected=\"selected\"" : ""; ?>>Pará</option>
										    <option value="PB" <?php echo ( $opcoes->estado == "PB" ) ? "selected=\"selected\"" : ""; ?>>Paraíba</option>
										    <option value="PR" <?php echo ( $opcoes->estado == "PR" ) ? "selected=\"selected\"" : ""; ?>>Paraná</option>
										    <option value="PE" <?php echo ( $opcoes->estado == "PE" ) ? "selected=\"selected\"" : ""; ?>>Pernambuco</option>
										    <option value="PI" <?php echo ( $opcoes->estado == "PI" ) ? "selected=\"selected\"" : ""; ?>>Piauí</option>
										    <option value="RJ" <?php echo ( $opcoes->estado == "RJ" ) ? "selected=\"selected\"" : ""; ?>>Rio de Janeiro</option>
										    <option value="RN" <?php echo ( $opcoes->estado == "RN" ) ? "selected=\"selected\"" : ""; ?>>Rio Grande do Norte</option>
										    <option value="RO" <?php echo ( $opcoes->estado == "RO" ) ? "selected=\"selected\"" : ""; ?>>Rondônia</option>
										    <option value="RS" <?php echo ( $opcoes->estado == "RS" ) ? "selected=\"selected\"" : ""; ?>>Rio Grande do Sul</option>
										    <option value="RR" <?php echo ( $opcoes->estado == "RR" ) ? "selected=\"selected\"" : ""; ?>>Roraima</option>
										    <option value="SC" <?php echo ( $opcoes->estado == "SC" ) ? "selected=\"selected\"" : ""; ?>>Santa Catarina</option>
										    <option value="SP" <?php echo ( $opcoes->estado == "SP" ) ? "selected=\"selected\"" : ""; ?>>São Paulo</option>
										    <option value="SE" <?php echo ( $opcoes->estado == "SE" ) ? "selected=\"selected\"" : ""; ?>>Sergipe</option>
										    <option value="TO" <?php echo ( $opcoes->estado == "TO" ) ? "selected=\"selected\"" : ""; ?>>Tocantins</option>
										</select>
									</label>
									<label>
										CEP<br>
										<input type="text" name="cep" style="width:100%" value="<?php echo $opcoes->cep; ?>">
									</label>
								</td>
							</tr> <?php
						break;
					}
					echo "</table>";
				} ?>
				<p class="submit" style="clear: both;">
					<input type="hidden" name="updated">
					<input type="submit" class="button-primary" value="Salvar Alterações" />
					<input type="hidden" name="modueco-settings-submit" value="Y" />
				</p>
			</form>
			<script type="text/javascript">
			</script>
			<p>Tema Wordpress <b>"<?php echo $theme_data["Name"] ?>"</b> por <b><a href="http://www.matsumurafidelis.com/" target="_blank">Matsumura ● Fidelis Publicidade</a></b></p>
		</div>
	</div>
<?php
}
function _get_wp_version() {
	global $wp_version;
	return $wp_version;
}
add_action( "admin_head", "theme_options_custom_scripts" );
function theme_options_custom_scripts() {
	if( function_exists( "wp_enqueue_media" ) && version_compare( _get_wp_version(), "3.5", ">=" ) ) {
		wp_enqueue_media();
	} else {
		wp_enqueue_script( "media-upload" );
		wp_enqueue_script( "thickbox" );
		wp_enqueue_style( "thickbox" );
	}
	wp_enqueue_style( "media" ); ?>
	<script type="text/javascript">
		!function ($) {
			$(function() {
				// Media Uploader
				$('#cmu-fabricacao').click(function(e) {
					e.preventDefault();
					var custom_uploader = wp.media({
						title: 'Selecionar imagem',
						button: { text: 'Salvar' },
						multiple: false
					})
					.on('select', function() {
						var attachment = custom_uploader.state().get('selection').first().toJSON();
						$('.cmu-fabricacao').val(attachment.url);
						console.log(attachment);
					})
					.open();
				});
				$('#cmu-tipos').click(function(e) {
					e.preventDefault();
					var custom_uploader = wp.media({
						title: 'Selecionar imagem',
						button: { text: 'Salvar' },
						multiple: false
					})
					.on('select', function() {
						var attachment = custom_uploader.state().get('selection').first().toJSON();
						$('.cmu-tipos').val(attachment.url);
						console.log(attachment);
					})
					.open();
				});
				$('#cmu-aplicacao').click(function(e) {
					e.preventDefault();
					var custom_uploader = wp.media({
						title: 'Selecionar imagem de fabricação',
						button: { text: 'Salvar' },
						multiple: false
					})
					.on('select', function() {
						var attachment = custom_uploader.state().get('selection').first().toJSON();
						$('.cmu-aplicacao').val(attachment.url);
						console.log(attachment);
					})
					.open();
				});
				$('#cmu-produtos-1').click(function(e) {
					e.preventDefault();
					var custom_uploader = wp.media({
						title: 'Selecionar imagem de fabricação',
						button: { text: 'Salvar' },
						multiple: false
					})
					.on('select', function() {
						var attachment = custom_uploader.state().get('selection').first().toJSON();
						$('.cmu-produtos-1').val(attachment.url);
						console.log(attachment);
					})
					.open();
				});
				$('#cmu-produtos-2').click(function(e) {
					e.preventDefault();
					var custom_uploader = wp.media({
						title: 'Selecionar imagem de fabricação',
						button: { text: 'Salvar' },
						multiple: false
					})
					.on('select', function() {
						var attachment = custom_uploader.state().get('selection').first().toJSON();
						$('.cmu-produtos-2').val(attachment.url);
						console.log(attachment);
					})
					.open();
				});
				$('#cmu-produtos-3').click(function(e) {
					e.preventDefault();
					var custom_uploader = wp.media({
						title: 'Selecionar imagem de fabricação',
						button: { text: 'Salvar' },
						multiple: false
					})
					.on('select', function() {
						var attachment = custom_uploader.state().get('selection').first().toJSON();
						$('.cmu-produtos-3').val(attachment.url);
						console.log(attachment);
					})
					.open();
				});
				$('#cmu-produtos-4').click(function(e) {
					e.preventDefault();
					var custom_uploader = wp.media({
						title: 'Selecionar imagem de fabricação',
						button: { text: 'Salvar' },
						multiple: false
					})
					.on('select', function() {
						var attachment = custom_uploader.state().get('selection').first().toJSON();
						$('.cmu-produtos-4').val(attachment.url);
						console.log(attachment);
					})
					.open();
				});
			});
		}(window.jQuery);
	</script> <?php
}





add_action( "init", "register_cpt_destaque" );
function register_cpt_destaque() {
	register_post_type( "destaque", array(
		"labels"				 => array(
			"name"				 => "Destaques",
			"singular_name"		 => "Destaque",
			"add_new"			 => "Novo Destaque",
			"all_items"			 => "Todos Destaques",
			"add_new_item"		 => "Adicionar Destaque",
			"edit"				 => "Alterar Destaque",
			"edit_item"			 => "Alterar Destaque",
			"new_item"			 => "Novo Destaque",
			"view"				 => "Visualizar Destaque",
			"view_item"			 => "Visualizar Destaque",
			"search_items"		 => "Buscar Destaques",
			"not_found"			 => "Nenhum resultado encontrado",
			"not_found_in_trash" => "Nenhum resultado encontrado na lixeira",
		),
		"description"				=> "Tipo de post para os Destaques do site.",
		"public"					=> true,
		"exclude_from_search"		=> true,
		"publicly_queryable"		=> true,
		"show_ui"					=> true,
		"show_in_nav_menus"			=> true,
		"show_in_menu"				=> true,
		"show_in_admin_bar"			=> true,
		"menu_position"				=> 5,
		"menu_icon"					=> null,
		"capability_type"			=> "post",
		"hierarchical"				=> false,
		"supports"					=> array( "title", "thumbnail" ),
		"taxonomies"				=> array( ),
		"rewrite"					=> array( "slug" => "destaque" ),
		"query_var"					=> "destaque",
		"can_export"				=> true,
	));
}
/**
 * Muda o texto da meta box "Imagem destacada"
**/
add_action( "do_meta_boxes", "change_destaque_image_box" );
function change_destaque_image_box() {
	remove_meta_box( "postimagediv", "destaque", "side" );
	add_meta_box( "postimagediv", __("Imagem Destacada <small style=\"color:#999999\">2000x600 pixels</small>"), "post_thumbnail_meta_box", "destaque", "side", "low" );
}
/**
 * Campos personalizados para Destaques
**/
add_action( "admin_init", "ui_link_destaque" );
function ui_link_destaque() {
	add_meta_box( "cf_link_destaque", "Link", "cf_link_destaque", "destaque", "normal", "high" );
}
function cf_link_destaque() {
	global $post;
	$link = get_post_meta( $post->ID, "link", true );
	$target = get_post_meta( $post->ID, "target", true ); ?>
	<label>
		Endereço do link<br>
		<input type="text" name="link" maxlength="255" style="width:100%" value="<?php echo $link; ?>">
	</label>
	<br><hr>
	<label><input type="checkbox" name="target" value="true" <?php verifica_valor_input( "true", $target, "checkbox" ); ?>> Abrir em uma nova janela</label> <?php
}
add_action( "save_post", "save_ui_destaque", 10, 2 );
function save_ui_destaque( $postID, $post ) {
	if ( $post->post_type == "destaque" ) {
		if( isset( $_POST["link"] ) ) { update_post_meta( $postID, "link", $_POST["link"] ); }
		if( isset( $_POST["target"] ) ) {
			update_post_meta( $postID, "target", $_POST["target"] );
		} else {
			update_post_meta( $postID, "target", "" );
		}
	}
}
add_filter( "manage_edit-destaque_columns", "cols_destaque" );
function cols_destaque( $cols ) {
	unset( $cols["date"] );
	$cols["link"] = "Link";
	$cols["date"] = "Data";
	return $cols;
}
add_filter( "manage_posts_custom_column", "populate_cols_destaque" );
function populate_cols_destaque( $cols ) {
	if( $cols == "link" ) {
		echo get_post_meta( get_the_ID(), "link", true );
	}
}



// Produtos
add_action( "init", "register_cpt_produto" );
function register_cpt_produto() {
	register_post_type( "produto", array(
		"labels"				 => array(
			"name"				 => "Produtos",
			"singular_name"		 => "Produto",
			"add_new"			 => "Novo Produto",
			"all_items"			 => "Todos Produtos",
			"add_new_item"		 => "Adicionar Produto",
			"edit"				 => "Alterar Produto",
			"edit_item"			 => "Alterar Produto",
			"new_item"			 => "Novo Produto",
			"view"				 => "Ver Produto",
			"view_item"			 => "Ver Produto",
			"search_items"		 => "Buscar Destaques",
			"not_found"			 => "Nenhum resultado encontrado",
			"not_found_in_trash" => "Nenhum resultado encontrado na lixeira",
		),
		"description"				=> "Tipo de post para os Produtos do site.",
		"public"					=> true,
		"exclude_from_search"		=> true,
		"publicly_queryable"		=> true,
		"show_ui"					=> true,
		"show_in_nav_menus"			=> true,
		"show_in_menu"				=> true,
		"show_in_admin_bar"			=> true,
		"menu_position"				=> 5,
		"menu_icon"					=> null,
		"capability_type"			=> "post",
		"hierarchical"				=> false,
		"supports"					=> array( "title", "editor", "thumbnail" ),
		"taxonomies"				=> array( ),
		"rewrite"					=> array( "slug" => "produto" ),
		"query_var"					=> "produto",
		"can_export"				=> true,
	));
}
add_action( "admin_init", "ui_video_produto" );
function ui_video_produto() {
	add_meta_box( "cf_video_produto", "Vídeo", "cf_video_produto", "produto", "normal", "high" );
}
function cf_video_produto() {
	global $post;
	$video = get_post_meta( $post->ID, "video", true ); ?>
	<label>
		Código do vídeo <small>http://www.youtube.com/watch?v=<b>BBqqbbR7-GE</b></small><br>
		<input type="text" name="video" maxlength="11" style="width:100%" value="<?php echo $video; ?>">
	</label> <?php
}
add_action( "save_post", "save_ui_produto", 10, 2 );
function save_ui_produto( $postID, $post ) {
	if ( $post->post_type == "produto" ) {
		if( isset( $_POST["video"] ) ) { update_post_meta( $postID, "video", $_POST["video"] ); }
	}
}
add_filter( "manage_edit-produto_columns", "cols_produto" );
function cols_produto( $cols ) {
	unset( $cols["date"] );
	$cols["video"] = "Vídeo";
	$cols["date"] = "Data";
	return $cols;
}
add_filter( "manage_posts_custom_column", "populate_cols_produto" );
function populate_cols_produto( $cols ) {
	if( $cols == "video" ) {
		echo get_post_meta( get_the_ID(), "video", true );
	}
}
