<?php get_header(); ?>

		<div role="main" class="main">

			<section class="page-top">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2>404 - Página não encontrada</h2>
						</div>
					</div>
				</div>
			</section>

			<div class="container">

				<section class="page-not-found">
					<div class="row">
						<div class="col-md-6 col-md-offset-1">
							<div class="page-not-found-main">
								<h2>404 <i class="icon icon-file"></i></h2>
								<p>Desculpe, esta página não existe.</p>
							</div>
						</div>
						<div class="col-md-4">
							<h4>Alguns links úteis</h4>
							<?php wp_nav_menu( array( "theme_location" => "global", "menu_id" => "menu404", "depth" => 4, "container" => false, "menu_class" => "nav nav-list primary", "walker" => new bootstrap_nav_walker())); ?>
						</div>
					</div>
				</section>

			</div>

		</div>

<?php get_footer(); ?>