<?php get_header(); ?>

		<div role="main" class="main">

			<section class="page-top">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2><?php the_title(); ?></h2>
						</div>
					</div>
				</div>
			</section>

			<div class="container">

				<div class="row">
					<div class="col-md-9">
						<div class="blog-posts single-post">

							<article class="post post-large blog-single-post">

								<div class="post-date">
									<span class="day"><?php the_time("d"); ?></span>
									<span class="month"><?php the_time("M"); ?></span>
								</div>

								<div class="post-content">

									<h2><?php the_title(); ?></h2>

									<div class="post-meta">
										<span><i class="icon icon-tag"></i> <?php the_category(", "); ?></span>
									</div>

									<?php the_content(); ?>


									<div class="post-block post-share">
										<h3><i class="icon icon-share"></i>Compartilhe este post</h3>
										<iframe src="//www.facebook.com/plugins/like.php?href=<?php the_permalink(); ?>&amp;width=500&amp;layout=standard&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:500px; height:21px;" allowTransparency="true"></iframe>
									</div>

								</div>
							</article>

						</div>
					</div>

					<div class="col-md-3 hidden-xs">
						<aside class="sidebar">

							<h4>Categorias</h4>
							<ul class="nav nav-list primary push-bottom"> <?php
								$terms = wp_get_post_terms( get_the_ID(), "category" );
								foreach ( $terms as $term ) {
									$categoria = $term->name;
									$link = get_term_link( $term, "category" );
									echo "<li><a href=\"" . $link . "\">" . $categoria . "</a></li>";
								} ?>
							</ul>

						</aside>
					</div>
				</div>

			</div>

		</div>

<?php get_footer(); ?>