<?php get_header(); ?>

		<div role="main" class="main">

			<section class="page-top">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2><?php the_title(); ?></h2>
						</div>
					</div>
				</div>
			</section>

			<div class="container">
				<div class="row">
					<ul class="portfolio-list isotope"> <?php
						$_produtos = array( "post_type" => "produto", "orderby" => "title", "order" => "ASC", "posts_per_page" => -1 );
						$produtos = new WP_Query(  $_produtos );
						if( $produtos->have_posts() ) while( $produtos->have_posts() ) {
							$produtos->the_post(); ?>
							<li class="col-md-3 isotope-item">
								<div class="portfolio-item img-thumbnail">
									<a href="<?php the_permalink(); ?>" class="thumb-info">
										<?php the_post_thumbnail( "350x350", array( "class" => "img-responsive", "alt" => "" ) ); ?>
										<span class="thumb-info-title">
											<span class="thumb-info-inner"><?php the_title(); ?></span>
										</span>
										<span class="thumb-info-action">
											<span title="<?php the_title(); ?>" href="<?php the_permalink(); ?>" class="thumb-info-action-icon"><i class="icon icon-link"></i></span>
										</span>
									</a>
								</div>
							</li><?php
						} ?>
					</ul>
				</div>
			</div>

		</div>

<?php get_footer(); ?>