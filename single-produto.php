<?php get_header(); ?>

		<div role="main" class="main">

			<section class="page-top">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2><?php the_title(); ?></h2>
						</div>
					</div>
				</div>
			</section>
			<div class="container">
				<!-- <div class="row">
					<div class="col-md-12">
						<h2><?php the_title(); ?></h2>
						<hr>
					</div>
				</div> -->
				<div class="row">
					<div class="col-md-3">
						<?php the_post_thumbnail( "350x350", array( "class" => "thumbnail" ) ); ?>
					</div>
					<div class="col-md-9"> <?php
						the_content();
						$video = get_post_meta( get_the_ID(), "video", true );
						if( !empty( $video ) ) { ?>
							<hr>
							<div class="video-container youtube">
								<iframe frameborder="0" allowfullscreen="" src="http://www.youtube.com/embed/<?php echo $video; ?>?showinfo=0&amp;wmode=opaque"></iframe>
							</div><?php
						} ?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div>
							<hr>
							<h3><i class="icon icon-share"></i>Compartilhe este post</h3>
							<iframe src="//www.facebook.com/plugins/like.php?href=<?php the_permalink(); ?>&amp;width=500&amp;layout=standard&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:500px; height:21px;" allowTransparency="true"></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>

<?php get_footer(); ?>