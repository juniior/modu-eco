<?php session_start();
//$session_exists = session_id();
//if ( empty( $session_exists ) ) { session_start(); }
header( "Expires: 0" );
header( "Connection: Keep-alive" );
header( "Cache-Control: max-age=0" );
header( "X-UA-Compatible: IE=edge,chrome=1" );
date_default_timezone_set( "America/Porto_Velho" );
setlocale( LC_ALL, "pt_BR", "pt_BR.iso-8859-1", "pt_BR.utf-8", "portuguese" ); ?>
<!-- Criação e desenvolvimento: Emiliano Matsumura e Rubens Fidelis --> 
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
<head>
	<meta charset="utf-8"> <?php
	global $post;
	if ( have_posts() ): while( have_posts() ): the_post(); endwhile; endif; ?>
	<meta property="fb:admins" content="100000117118815"> <?php
	if (is_single()) { ?>
		<meta property="og:url" content="<?php the_permalink(); ?>">
		<meta property="og:title" content="<?php single_post_title(""); ?>">
		<meta property="og:site_name" content="<?php bloginfo( "name" ); ?>">
		<meta property="og:description" content="<?php echo strip_tags( get_the_excerpt( $post->ID ) ); ?>">
		<meta property="og:type" content="article">
		<meta property="og:image" content="<?php #echo cdn_resource( "imgs/logo-governo-fb.png" ); ?>"> <?php
	} else { ?>
		<meta property="og:url" content="<?php echo get_option( "home" ); ?>/">
		<meta property="og:title" content="<?php bloginfo( "name" ); ?>">
		<meta property="og:site_name" content="<?php bloginfo( "name" ); ?>">
		<meta property="og:description" content="<?php bloginfo( "description" ); ?>">
		<meta property="og:type" content="website">
		<meta property="og:image" content="<?php #echo cdn_resource( "imgs/logo-governo-fb.png" ); ?>"> <?php
	} ?>
	<title> <?php
		if ( function_exists( "is_tag" ) && is_tag() ) {
			single_tag_title( "Tag para &quot;" );
			echo "&quot; - ";
		} else if ( is_archive() ) {
			echo "Categorias - ";
			wp_title("");
			echo " - ";
		} elseif ( is_search() ) {
			echo "Busca por &quot;" . wp_specialchars( $s ) . "&quot; - ";
		} elseif ( !( is_404() ) && ( is_single() ) || ( is_page() ) ) {
			wp_title("");
			echo " - ";
		} elseif ( is_404() ) {
			echo "Página não encontrada - ";
		}
		if ( is_home() ) {
			bloginfo( "name" );
			echo " - ";
			bloginfo( "description" );
		} else {
			bloginfo( "name" );
		}
		if ( $paged > 1 ) {
			echo " - Página " . $paged;
		} ?>
	</title>
	<meta name="description" content="Modu-eco - Tijolos ecológicos. Obras mais sustentáveis.">
	<meta name="keywords" content="modu-eco, sustentabilidade, ecologico, tijolo, obras, porto, velho, rondônia">
	<meta name="author" content="Modu-eco">
	<meta name="robots" content="index,follow">
	<meta name="viewport" content="width=device-width">
	<?php wp_head(); ?>
	<?php if( is_category() || is_single() ) { echo "<link rel=\"stylesheet\" href=\"" . get_bloginfo( "template_url" ) . "/css/theme-blog.css\" media=\"screen\">"; } ?>
	<!--[if IE]>
		<link rel="stylesheet" href="<?php bloginfo( "template_url" ); ?>/css/ie.css">
	<![endif]-->
	<!--[if lte IE 8]>
		<script src="<?php bloginfo( "template_url" ); ?>/vendor/respond.js"></script>
	<![endif]-->
</head>
<body>
	<div id="fb-root"></div><script>(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=548129458554788"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk'));</script>
	<div class="body">
		<!-- Cabeçalho -->
		<header class="single-menu flat-menu">
			<div class="container">
				<h1 class="logo">
					<a href="<?php bloginfo( "home" ); ?>">
						<img alt="Porto" src="<?php bloginfo( "template_url" ); ?>/img/logo.png">
					</a>
				</h1>
				<button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse">
					<i class="icon icon-bars"></i>
				</button>
			</div>
			<div class="navbar-collapse nav-main-collapse collapse">
				<div class="container">
					<div class="social-icons">
						<ul class="social-icons">
							<li class="facebook"><a href="http://www.facebook.com/moduecopvh" target="_blank" title="Facebook">Facebook</a></li>
						</ul>
					</div>
					<nav class="nav-main mega-menu">
						<?php wp_nav_menu( array( "theme_location" => "global", "menu_id" => "mainMenu", "depth" => 4, "container" => false, "menu_class" => "nav nav-pills nav-main", "walker" => new bootstrap_nav_walker())); ?>
					</nav>
				</div>
			</div>
		</header>
		<!-- /Cabeçalho -->
