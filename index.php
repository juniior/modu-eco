<?php get_header(); $_opcoes = rawJSON( "settings.json" ); ?>

		<div role="main" class="main">
			<div id="content" class="content full">

				<!-- Slider -->
				<div class="slider-container">
					<div class="slider" id="revolutionSlider">
						<ul> <?php
							$_destaques = array( "post_type" => "destaque", "orderby" => "date", "order" => "DESC", "posts_per_page" => 3 );
							$destaques = new WP_Query( $_destaques );
							if( $destaques->have_posts() ) while( $destaques->have_posts() ) {
								$destaques->the_post(); ?>
								<li data-transition="fade"><?php
									$link = get_post_meta( get_the_ID(), "link", true );
									$target = get_post_meta( get_the_ID(), "target", true );
									if( !empty( $link ) ) {
										echo "<a class=\"tp-caption main-label sft stb start\" data-x=\"0\" data-y=\"0\" data-speed=\"300\" data-start=\"0\" data-easing=\"easeOutExpo\" style=\"width:100%;height:100%;visibility:visible;display:block\" href=\"".$link."\"";
										if( $target ) { echo " target=\"_blank\""; }
										echo "></a>";
									}
									$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), "full" ); ?>
									<img src="<?php echo $thumbnail[0]; ?>" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
								</li><?php
							} ?>
						</ul>
					</div>
				</div>
				<!-- /Slider -->

				<div id="frase-destaque" class="container">

					<div class="row center">
						<div class="col-md-12">
							<h2 class="short word-rotator-title">
								Os produtos Modu-eco são
								<strong class="inverted">
									<span class="word-rotate">
										<span class="word-rotate-items">
											<span>especialmente</span>
											<span>extremamente</span>
											<span>incrivelmente</span>
										</span>
									</span>
								</strong>
								ecológicos.
							</h2>
							<p class="featured lead">
								Tijolos ecológicos e consultoria em projetos ecológicos e autosustentáveis!
							</p>
						</div>
					</div>

				</div>

				<!-- Produtos -->
				<div class="home-concept">
					<div class="container">

						<div class="row center">
							<span class="sun"></span>
							<span class="cloud"></span>
							<div class="col-md-2 col-md-offset-1">
								<div class="process-image" data-appear-animation="bounceIn"> <?php
									if( isset( $_opcoes->destaques->link_fabricacao ) && $_opcoes->destaques->link_fabricacao != "#" ) { ?>
										<a href="<?php echo $_opcoes->destaques->link_fabricacao; ?>">
											<img src="<?php echo $_opcoes->destaques->fabricacao; ?>" alt="Fabricação" />
										</a> <?php
									} else { ?>
										<img src="<?php echo $_opcoes->destaques->fabricacao; ?>" alt="Fabricação" /> <?php
									} ?>
									<strong>Fabricação</strong>
								</div>
							</div>
							<div class="col-md-2">
								<div class="process-image" data-appear-animation="bounceIn" data-appear-animation-delay="200"> <?php
									if( isset( $_opcoes->destaques->link_tipos ) && $_opcoes->destaques->link_tipos != "#" ) { ?>
										<a href="<?php echo $_opcoes->destaques->link_tipos; ?>">
											<img src="<?php echo $_opcoes->destaques->tipos; ?>" alt="Tipos" />
										</a> <?php
									} else { ?>
										<img src="<?php echo $_opcoes->destaques->tipos; ?>" alt="Tipos" /> <?php
									} ?>
									<strong>Tipos</strong>
								</div>
							</div>
							<div class="col-md-2">
								<div class="process-image" data-appear-animation="bounceIn" data-appear-animation-delay="400"> <?php
									if( isset( $_opcoes->destaques->link_aplicacao ) && $_opcoes->destaques->link_aplicacao != "#" ) { ?>
										<a href="<?php echo $_opcoes->destaques->link_aplicacao; ?>">
											<img src="<?php echo $_opcoes->destaques->aplicacao; ?>" alt="Aplicação" />
										</a> <?php
									} else { ?>
										<img src="<?php echo $_opcoes->destaques->aplicacao; ?>" alt="Aplicação" /> <?php
									} ?>
									<strong>Aplicação</strong>
								</div>
							</div>

							<div class="col-md-2 col-md-offset-1">
								<div class="project-image">
									<div id="fcSlideshow" class="fc-slideshow">
										<ul class="fc-slides"> <?php
											for( $i = 0; $i < 4; $i++ ) { ?> <?php
												if( isset( $_opcoes->destaques->link_produtos[$i] ) && $_opcoes->destaques->link_produtos[$i] != "#" ) { ?>
													<li><a href="<?php echo $_opcoes->destaques->link_produtos[$i]; ?>">
														<img class="img-responsive" src="<?php echo $_opcoes->destaques->produtos[$i]; ?>" alt="Produtos" />
													</a></li> <?php
												} else { ?>
													<li><img class="img-responsive" src="<?php echo $_opcoes->destaques->produtos[$i]; ?>" alt="Produtos" /></li> <?php
												}
											} ?>
										</ul>
									</div>
									<strong class="our-work">Produtos</strong>
								</div>
							</div>
							
						</div>

					</div>
				</div>
				<!-- /Produtos -->

				<div class="container">

					<div class="row">
						<hr class="tall" />
					</div>

				</div>

				<!-- Vantagens -->
				<div class="container">
					<div class="row">
						<div class="col-md-5">
							<h2>As <strong>Vantagens</strong></h2>
							<p><?php echo $_opcoes->vantagens->descricao; ?></p>
						</div>
						<div class="col-md-7">
							<div class="progress-bars"> <?php
								for( $i = 0; $i < 4; $i++ ) { ?>
									<div class="progress-label"> <?php
										if( isset( $_opcoes->vantagens->link[$i] ) && $_opcoes->vantagens->link[$i] != "#" ) { ?>
											<a href="<?php echo $_opcoes->vantagens->link[$i]; ?>">
												<span><?php echo $_opcoes->vantagens->titulo[$i]; ?></span>
											</a> <?php
										} else { ?>
											<span><?php echo $_opcoes->vantagens->titulo[$i]; ?></span> <?php
										} ?>
									</div>
									<div class="progress">
										<div class="progress-bar progress-bar-primary" data-appear-progress-animation="<?php echo $_opcoes->vantagens->porcentagem[$i]; ?><?php echo ( $i == 0 ) ? "%" : ""; ?>" <?php echo( $i > 0 ) ? "data-appear-animation-delay=\"".($i*300)."\"" : ""; ?>>
											<span class="progress-bar-tooltip"><?php echo $_opcoes->vantagens->porcentagem[$i]; ?><?php echo ( $i == 0 ) ? "%" : ""; ?></span>
										</div>
									</div> <?php
								} ?>
							</div>
						</div>
					</div>
					<!-- <hr class="tall" /> -->
				</div>
				<!-- /Vantagens -->

				<!-- Serviços - ->
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2>Nossos <strong>Serviços</strong></h2>
							<div class="row"> <?php
								/*for( $i = 0; $i < 3; $i++ ) { ?>
									<div class="col-md-4">
										<div class="feature-box-info"><?php
											if( isset( $_opcoes->servicos->link[$i] ) && $_opcoes->servicos->link[$i] != "#" ) { ?>
												<h4 class="shorter"><a href="<?php echo $_opcoes->servicos->link[$i]; ?>"><?php echo $_opcoes->servicos->titulo[$i]; ?></a></h4><?php
											} else { ?>
												<h4 class="shorter"><?php echo $_opcoes->servicos->titulo[$i]; ?></h4><?php
											} ?>
											<p class="tall"><?php echo $_opcoes->servicos->descricao[$i]; ?></p>
										</div>
									</div> <?php
								}*/ ?>
							</div>
						</div>
					</div>
				</div>
				<!- - Serviços -->

				<!-- Blog -->
				<section class="footer">
					<div class="container">
						<div class="row">

							<hr class="tall">

							<div class="col-md-12">
								<div class="recent-posts">
									<h2><strong>Blog</strong></h2>
									<div class="row">
										<div>
											<div> <?php
												$_posts = array( "post_type" => "post", "posts_per_page" => 4, "orderby" => "date", "order" => "DESC", "category_name" => "blog" );
												$posts = new WP_Query( $_posts );
												if( $posts->have_posts() ) while( $posts->have_posts() ) {
													$posts->the_post(); ?>
													<div class="col-md-3">
														<article>
															<div class="date">
																<span class="day"><?php the_time("d"); ?></span>
																<span class="month"><?php the_time("M"); ?></span>
															</div>
															<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
															<p><?php echo resumo( get_the_excerpt(), 20 ); ?>...</p><a href="<?php the_permalink(); ?>" class="read-more">leia mais <i class="icon icon-angle-right"></i></a>
														</article>
													</div> <?php
												} ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- /Blog -->
			</div>
		</div>

<?php get_footer(); ?>