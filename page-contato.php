<?php get_header(); $_opcoes = rawJSON( "settings.json" ); ?>

		<div role="main" class="main">

			<section class="page-top">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2><?php the_title(); ?></h2>
						</div>
					</div>
				</div>
			</section>

			<!-- Google Maps -->
			<!-- <div id="googlemaps" class="google-map hidden-xs"></div> -->
<iframe id="googlemaps" class="google-map hidden-xs"  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3943.317983436766!2d-63.87906958462888!3d-8.756122393710507!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x92325cb0090cfca3%3A0xab44133189bf09e0!2sAv.+Jos%C3%A9+Vieira+Ca%C3%BAla%2C+3791+-+Igarap%C3%A9%2C+Porto+Velho+-+RO!5e0!3m2!1spt-BR!2sbr!4v1491849771405" width="2000" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>

			<div class="container">

				<div class="row">
					<div class="col-md-6"> <?php
						require 'php-mailer/PHPMailerAutoload.php';
						if( isset( $_POST["ct_nome"] ) ) {
						  $name    = $_POST["ct_nome"];
						  $email   = $_POST["ct_email"];
						  $subject = ( !empty( $_POST["ct_assunto"] ) ) ? "Contato - ".$_POST["ct_assunto"] : "Contato";
						  $message = $_POST["ct_mensagem"];
						  // Mensagem
						  $msg  = "Um visitante do site ".get_bloginfo("name")." acaba de enviar a seguinte mensagem:\r\n\n";
						  $msg  .= "$message\r\n\n";
						  $msg  .= "Para responder $name, envie email para [$email].";
						  // Envio
						  $mail = new PHPMailer();
						  $mail->IsMail();
						  $mail->AddAddress( $_opcoes->email );
						  $mail->From     = $email;
						  $mail->FromName = $name;
						  $mail->Sender   = $email;
						  $mail->AddReplyTo( $email, utf8_decode( $name ) );
						  $mail->IsHTML( false );
						  $mail->Subject  = utf8_decode( $subject );
						  $mail->Body     = utf8_decode( $msg );
						  if( $mail->Send() ) {
						    echo "<div class=\"alert alert-success\"><strong>Sucesso!</strong> Sua mensagem foi enviada.</div>";
						  } else {
						    echo "<div class=\"alert alert-danger\"><strong>Erro!</strong> A mensagem não foi enviada.</div>";
						  }
						} ?>

						<h2 class="short"><strong>Fale</strong> Conosco</h2>
						<form action="<?php the_permalink(); ?>" method="POST">
							<div class="row">
								<div class="form-group">
									<div class="col-md-6">
										<label>Nome*</label>
										<input type="text" value="" data-msg-required="Por favor preencha seu nome." maxlength="100" class="form-control" name="ct_nome" id="ct_nome" required>
									</div>
									<div class="col-md-6">
										<label>Email*</label>
										<input type="email" value="" data-msg-required="Por favor preencha seu email." data-msg-email="Por favor preencha um email válido." maxlength="100" class="form-control" name="ct_email" id="ct_email" required>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<div class="col-md-12">
										<label>Assunto</label>
										<input type="text" value="" data-msg-required="Por favor preencha um assunto." maxlength="100" class="form-control" name="ct_assunto" id="ct_assunto">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<div class="col-md-12">
										<label>Mensagem*</label>
										<textarea maxlength="5000" data-msg-required="Por favor preencha sua mensagem." rows="10" class="form-control" name="ct_mensagem" id="ct_mensagem" required></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<input type="submit" value="Enviar mensagem" class="btn btn-primary btn-lg" data-loading-text="Carregando...">
								</div>
							</div>
						</form>
					</div>
					<div class="col-md-6">

						<h4>O <strong>Escritório</strong></h4>
						<ul class="list-unstyled">
							<li><i class="icon icon-map-marker"></i> <strong>Endereço:</strong> <?php echo $_opcoes->rua.", ".$_opcoes->numero." - Bairro ".$_opcoes->bairro." - ".$_opcoes->cidade.", ".$_opcoes->estado; ?></li>
							<li><i class="icon icon-phone"></i> <strong>Telefone:</strong> <?php echo $_opcoes->telefone; ?></li>
							<li><i class="icon icon-envelope"></i> <strong>Email:</strong> <a href="mailto:<?php echo $_opcoes->email; ?>"><?php echo $_opcoes->email; ?></a></li>
						</ul>

						<!-- <hr />

						<h4>Horário de <strong>Atendimento</strong></h4>
						<ul class="list-unstyled">
							<li><i class="icon icon-time"></i> Segunda à Sexta das 09:00 às 17:00</li>
							<li><i class="icon icon-time"></i> Sábado das 09:00 às 13:00</li>
						</ul> -->

					</div>

				</div>

			</div>

		</div>

<?php get_footer(); ?>

<script src="http://maps.google.com/maps/api/js?sensor=false"></script>

<script src="<?php bloginfo( "template_url" ); ?>/vendor/jquery.gmap.js"></script>

<script>

	/*
	Map Settings

		Find the Latitude and Longitude of your address:
			- http://universimmedia.pagesperso-orange.fr/geo/loc.htm
			- http://www.findlatitudeandlongitude.com/find-address-from-latitude-and-longitude/

	*/

	// Map Markers
	var mapMarkers = [{
		address: "<?php echo $_opcoes->rua.", ".$_opcoes->numero." - Bairro ".$_opcoes->bairro." - ".$_opcoes->cidade.", ".$_opcoes->estado; ?>",
		html: "<strong>Escritório <?php echo $_opcoes->cidade.", ".$_opcoes->estado; ?></strong><br><?php echo $_opcoes->rua.", ".$_opcoes->numero; ?>",
		icon: {
			image: "img/pin.png",
			iconsize: [26, 46],
			iconanchor: [12, 46]
		},
		popup: true
	}];

	// Map Initial Location
	var initLatitude = 0;
	var initLongitude = 0;

	// Map Extended Settings
	var mapSettings = {
		controls: {
			panControl: true,
			zoomControl: true,
			mapTypeControl: true,
			scaleControl: true,
			streetViewControl: true,
			overviewMapControl: true
		},
		scrollwheel: false,
		markers: mapMarkers,
		latitude: initLatitude,
		longitude: initLongitude,
		zoom: 16
	};

	var map = $("#googlemaps").gMap(mapSettings);

	// Map Center At
	var mapCenterAt = function(options, e) {
		e.preventDefault();
		$("#googlemaps").gMap("centerAt", options);
	}

</script>