<?php get_header(); ?>

		<div role="main" class="main">

			<section class="page-top">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2><?php single_cat_title(); ?></h2>
						</div>
					</div>
				</div>
			</section>

			<div class="container">

				<div class="row">
					<div class="col-md-9">
						<div class="blog-posts"> <?php
							if( have_posts() ) while( have_posts() ) {
								the_post(); ?>
								<article class="post post-large">
									<div class="post-date">
										<span class="day"><?php the_time("d"); ?></span>
										<span class="month"><?php the_time("M") ?></span>
									</div>
									<div class="post-content">
										<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
										<div><?php the_excerpt(); ?></div>
										<div class="post-meta">
											<span><i class="icon icon-tag"></i> <?php the_category( ", " ); ?></span>
											<a href="<?php the_permalink(); ?>" class="btn btn-xs btn-primary pull-right">Leia mais...</a>
										</div>
									</div>
								</article> <?php
							}
							if ( function_exists( "pagination" ) ) pagination( $wp_query->max_num_pages ); ?>
						</div>
					</div>

					<div class="col-md-3">
						<aside class="sidebar">

							<h4>Categorias</h4>
							<ul class="nav nav-list primary push-bottom"> <?php
								$terms = wp_get_post_terms( get_the_ID(), "category" );
								foreach ( $terms as $term ) {
									$categoria = $term->name;
									$link = get_term_link( $term, "category" );
									echo "<li><a href=\"" . $link . "\">" . $categoria . "</a></li>";
								} ?>
							</ul>

						</aside>
					</div>
				</div>

			</div>

		</div>

<?php get_footer(); ?>

<script>
	$(".menu-item-object-category ").addClass("active");
</script>