<?php
//PhP Mailer
require 'php-mailer/PHPMailerAutoload.php';
if( isset( $_POST["name"] ) ) {
  $name    = $_POST["name"];
  $email   = $_POST["email"];
  $subject = ( !empty( $_POST["subject"] ) ) ? $_POST["subject"] : "Contato";
  $message = $_POST["message"];
  // Mensagem
  $msg  = "Um visitante do site www.modueco.com.br acaba de enviar a seguinte mensagem:\r\n\n";
  $msg  .= "$message\r\n\n";
  $msg  .= "Para responder $name, envie email para [$email].";
  // Envio
  $mail = new PHPMailer();
  $mail->IsMail();
  $mail->AddAddress( "contato@modueco.com.br" );
  $mail->From     = $email;
  $mail->FromName = $name;
  $mail->Sender   = $email;
  $mail->AddReplyTo( $email, utf8_decode( $name ) );
  $mail->IsHTML( false );
  $mail->Subject  = utf8_decode( $subject );
  $mail->Body     = utf8_decode( $msg );
  if( $mail->Send() ) {
    echo "Mensagem enviada com sucesso!";
  } else {
    echo "Desculpe, a mensagem não pode ser enviada.";
  }
}