		<!-- Rodapé -->
		<footer class="short" id="footer">
			<div class="container">
				<div class="row">
					<!-- Sobre -->
					<div class="col-md-4">
						<h4>Sobre</h4>
						<?php $_sobre = get_page_by_path( "sobre" ); ?>
						<p>Uma das metas da Modu Eco, é a de incentivar a conscientização de obras mais sustentáveis e praticá-las, aonde haja no mínimo 2 sistemas do projeto racionalizados (instalações hidráulicas utilizando dispositivos economizadores; aproveitamento de água pluvial; tratamento de efluentes por zona de raízes e reaproveitamento da água tratada para irrigação, geração de energia fotovoltaica, entre outros).</p>
						<a href="<?php echo get_page_link( $_sobre->ID ); ?>" class="btn-flat btn-xs">Leia mais <i class="icon icon-arrow-right"></i></a>
					</div>
					<!-- /Sobre -->
					<!-- Novidades -->
					<div class="col-md-4">
						<h4>Novidades</h4>
						<ul class="nav nav-list primary push-bottom"> <?php
							$_novidades = array( "post_type" => "post", "posts_per_page" => 4, "orderby" => "date", "order" => "DESC", "category_name" => "novidades" );
							$novidades = new WP_Query( $_novidades );
							if( $novidades->have_posts() ) while( $novidades->have_posts() ) {
								$novidades->the_post(); ?>
								<li><a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li><?php
							} ?>
						</ul>
					</div>
					<!-- /Novidades -->
					<!-- Facebook -->
					<div class="col-md-4">
						<h4>Facebook</h4>
						<div class="fb-like-box" data-href="http://www.facebook.com/moduecopvh" data-width="365" data-height="235" data-colorscheme="dark" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
					</div>
					<!-- /Facebook -->
				</div>
			</div>
			<!-- Copyright -->
			<div class="footer-copyright">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
						<a href="mailto:emiliano.mm@gmail.com">
							<img class="pull-right" src="<?php bloginfo("template_url"); ?>/img/logo-mf.png" alt="MF"></a> 
							<p>© <?php echo date("Y"); ?> - <?php bloginfo("name"); ?> - Todos os direitos reservados.</p>
						</div>
					</div>
				</div>
			</div>
			<!-- Copyright -->
		</footer>
		<!-- /Rodapé -->
	</div>
	<?php wp_footer(); ?>
</body>
</html>